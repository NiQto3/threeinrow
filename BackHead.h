#pragma once
#include <iostream>
#include <stdio.h>     
#include <stdlib.h>     
#include <time.h>

void StartGame();
bool Check_Row();
bool Check_Any_Move();
void Check_Board();
bool Make_Move();
void DropGems();
void FillVoids();
void OpenMatrix(int(&Matrix)[12][12]);