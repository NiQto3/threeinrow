#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "Draw.h"
#include <wingdi.h>


//Draw background
void DrawBackgrownd(HDC hdc) {
	HPEN hPen;
	RECT r1{ };
	hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 255));
	SelectObject(hdc, hPen);
	Rectangle(hdc, 0, 0, 1920, 1080);
	r1 = { 0, 0, 1920, 1080 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(116, 61, 199)));
	DeleteObject(hPen);
}

//Drawing gem
void Gem(HDC hdc, int X, int Y, HBRUSH brush) {
	POINT poly[6] = { { X + 40,Y + 10 },{ X + 70, Y + 20 },{X + 70, Y + 60},{ X + 40, Y + 70 },{ X + 10, Y + 60 },{ X + 10, Y + 20 } };
	SelectObject(hdc, brush);
	Polygon(hdc, poly, 6);
	HPEN lPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
	SelectObject(hdc, lPen);
	MoveToEx(hdc, X + 40, Y + 10, NULL);
	LineTo(hdc, X + 40, Y + 70);
	MoveToEx(hdc, X + 10, Y + 20, NULL);
	LineTo(hdc, X + 40, Y + 30);
	MoveToEx(hdc, X + 10, Y + 60, NULL);
	LineTo(hdc, X + 40, Y + 50);
	MoveToEx(hdc, X + 70, Y + 20, NULL);
	LineTo(hdc, X + 40, Y + 30);
	MoveToEx(hdc, X + 70, Y + 60, NULL);
	LineTo(hdc, X + 40, Y + 50);
	DeleteObject(lPen);
}

void BoldGem(HDC hdc, int X, int Y) {
	HPEN lPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
	SelectObject(hdc, lPen);
	X = X * 80 + 480;
	Y = Y * 80 + 80;
	MoveToEx(hdc, X + 40, Y + 10, NULL);
	LineTo(hdc, X + 40, Y + 70);
	MoveToEx(hdc, X + 10, Y + 20, NULL);
	LineTo(hdc, X + 40, Y + 30);
	MoveToEx(hdc, X + 10, Y + 60, NULL);
	LineTo(hdc, X + 40, Y + 50);
	MoveToEx(hdc, X + 70, Y + 20, NULL);
	LineTo(hdc, X + 40, Y + 30);
	MoveToEx(hdc, X + 70, Y + 60, NULL);
	LineTo(hdc, X + 40, Y + 50);
	MoveToEx(hdc, X + 40, Y + 10, NULL);
	LineTo(hdc, X + 70, Y + 20);
	LineTo(hdc, X + 70, Y + 60);
	LineTo(hdc, X + 40, Y + 70);
	LineTo(hdc, X + 10, Y + 60);
	LineTo(hdc, X + 10, Y + 20);
	LineTo(hdc, X + 40, Y + 10);
	DeleteObject(lPen);
}

void DrawMatrix(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 255));
	SelectObject(hdc, hPen);
	int i = 0, j = 0, x1 = 0, y1 = 0, x2 = 0, y2 = 0, bug = 0;

	Rectangle(hdc, 480, 80, 1440, 1040); //960 x 960 / 12 x 12 sokets / 80 x 80 / top row is used in animation
	RECT RMatrix = { 480, 80, 1440, 1040 };
	FillRect(hdc, &RMatrix, (HBRUSH)CreateSolidBrush(RGB(255, 255, 255)));
	x1 = 480; x2 = 1439;  //position of the first line inside
	for ( i = 1; i < 12; i++)
	{
		y1 = i * 80 + 80;
		MoveToEx(hdc, x1, y1, NULL);
		LineTo(hdc, x2, y1);
	}
	y1 = 80; y2 = 1039;
	for (i = 1; i < 12; i++)
	{
		x1 = i * 80 + 480;
		MoveToEx(hdc, x1, y1, NULL);
		LineTo(hdc, x1, y2);
	}
	HBRUSH hBr, hBrRed, hBrGreen, hBrBlue, hBrPink, hBrOran;
	hBrRed = CreateSolidBrush(RGB(255, 0, 0));
	hBrGreen = CreateSolidBrush(RGB(15, 247, 85));
	hBrBlue = CreateSolidBrush(RGB(22, 47, 204));
	hBrPink = CreateSolidBrush(RGB(201, 22, 204));
	hBrOran = CreateSolidBrush(RGB(204, 116, 22));
	hPen = CreatePen(PS_SOLID, 0, RGB(255, 255, 255));
	SelectObject(hdc, hPen);
	hBr = hBrRed;
	FILE* Src = 0x00;
	fopen_s(&Src, "GameDesk.txt", "r");
	if (Src)
	{
		for (i = 0; i < 12; i++)
			for (j = 0; j < 12; j++)
			{
				x1 = i * 80 + 80;
				y1 = j * 80 + 480;//red green blue pink orange
				fscanf_s(Src, "%d", &bug);
				switch (bug)
				{
				case 1:
				{
					hBr = hBrRed;
					Gem(hdc, y1, x1, hBr);
					break;
				}
				case 2:
				{
					hBr = hBrGreen;
					Gem(hdc, y1, x1, hBr);
					break;
				}
				case 3:
				{
					hBr = hBrBlue;
					Gem(hdc, y1, x1, hBr);
					break;
				}
				case 4:
				{
					hBr = hBrPink;
					Gem(hdc, y1, x1, hBr);
					break;
				}
				case 5:
				{
					hBr = hBrOran;
					Gem(hdc, y1, x1, hBr);
					break;
				}
				default:
					break;
				}
			}
		fclose(Src);
	}
	DeleteObject(hBr);
	DeleteObject(hBrRed);
	DeleteObject(hBrBlue);
	DeleteObject(hBrGreen);
	DeleteObject(hBrPink);
	DeleteObject(hBrOran);
	DeleteObject(hPen);

	hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 255));
	SelectObject(hdc, hPen);
	Rectangle(hdc, 480, 0, 1440, 80);//Score rectangle
	RECT r = { };
	HFONT hFont = {};
	FILE* FScore = 0x00;
	unsigned long int Score = 0;
	char CScore[11] = {};
	fopen_s(&FScore, "Score.txt", "r");
	if (FScore)
	{
		fscanf_s(FScore, "%u", &Score);
		_ultoa_s(Score, CScore, 11, 10);
		fclose(FScore);
	}
	r = { 485, 5, 1435, 75 };
	FillRect(hdc, &r, (HBRUSH)CreateSolidBrush(RGB(255, 255, 255)));
	DeleteObject(hPen);
	hFont = CreateFont(48, 0, 0, 0, 700, TRUE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Impact"));
	SelectObject(hdc, hFont);
	TextOut(hdc, 490, 5, L"Score: ", 7);
	TextOutA(hdc, 700, 5, CScore, strlen(CScore));
	DeleteObject(hFont);
	fopen_s(&Src, "Dots.txt", "r");
	if (Src)
	{
		fscanf_s(Src, "%d", &x1);
		if (!feof(Src))
		{
			fscanf_s(Src, "%d", &y1);
			fscanf_s(Src, "%d", &x2);
			if (feof(Src)) {
				BoldGem(hdc, x1, y1);
			}
		}
		fclose(Src);
	}
}

void DrawMtMove(HDC hdc) {
	/*HPEN hPen;
	hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 255));
	SelectObject(hdc, hPen);
	MoveToEx(hdc, 100, 100, NULL);
	LineTo(hdc, 150, 150);
	LineTo(hdc, 50, 200);
	LineTo(hdc, 100, 100);*/
}