#include <iostream>
#include <stdio.h>     
#include <stdlib.h>     
#include <time.h>
using namespace std;


//int GameBoard[12][12]; //board with gems
//unsigned long long int Score;

void OpenMatrix(int (&Matrix)[12][12]) {
	FILE* Src = 0x00;
	int i = 0, k = 0, buff = 0;
	fopen_s(&Src, "GameDesk.txt", "r");
	if (Src)
	{
		for (i = 0; i < 12; i++)
			for (k = 0; k < 12; k++) 
			{
				fscanf_s(Src, "%d", &buff);
				Matrix[i][k] = buff;
			}
		fclose(Src);
	}
}

void WriteMartix(int(&Matrix)[12][12]) {
	FILE* Src = 0x00;
	int i = 0, k = 0, buff = 0;
	fopen_s(&Src, "GameDesk.txt", "w+");
	if (Src)
	{
		for (i = 0; i < 12; i++)
			for (k = 0; k < 12; k++)
			{
				buff = Matrix[i][k];
				fprintf(Src, "%d\n", buff);			
			}
		fclose(Src);
	}
}

//Before start make no rows(Checked)//ADD OPEN AND WRITE MATRIX FUNC
void Check_Board() {
	int i = 0, k = 0, tmp = 1, buff = 0;
	int Game_Board[12][12] = {};
	OpenMatrix(Game_Board);

	while (tmp == 1)
	{
		for (i = 0; i < 12; i++)
			for (k = 0; k < 10; k++)
				if ((Game_Board[i][k] == Game_Board[i][k + 1]) && (Game_Board[i][k] == Game_Board[i][k + 2]))
					while (Game_Board[i][k] == Game_Board[i][k + 1])
						Game_Board[i][k] = rand() % 5 + 1;
		tmp = 0;
		for (k = 0; k < 12; k++)
			for (i = 0; i < 10; i++)
				if ((Game_Board[i][k] == Game_Board[i + 1][k]) && (Game_Board[i][k] == Game_Board[i + 2][k])) {
					while (Game_Board[i][k] == Game_Board[i + 2][k])
						Game_Board[i][k] = rand() % 5 + 1;
					tmp = 1;
				}
	}

	//Write back matrix in file
	WriteMartix(Game_Board);
}

//Check is it any possibility to make 3 in row. If there are moves then TRUE(Checked)
bool Check_Any_Move() {
	int i = 0, k = 0, flag = 0, buff = 0;
	int Game_Board[12][12] = {};

	OpenMatrix(Game_Board);

	for (i = 0; i < 12; i++)
		for (k = 0; k < 10; k++)
			if (Game_Board[i][k] == Game_Board[i][k + 2])
				switch (i) {
				case 0: {
					if (Game_Board[i][k] == Game_Board[i + 1][k + 1]) {
						return true;
					}
					else {
						break;
					}
				}
				case 12: {
					if (Game_Board[i][k] == Game_Board[i - 1][k + 1]) {
						return true;
					}
					else {
						break;
					}
				}
				default: {
					if ((Game_Board[i][k] == Game_Board[i + 1][k + 1]) || (Game_Board[i][k] == Game_Board[i - 1][k + 1]))
						return true;
				}
				}
	for (k = 0; k < 12; k++)
		for (i = 0; i < 10; i++)
			switch (k) {
			case 0: {
				if (Game_Board[i][k] == Game_Board[i + 1][k + 1])
				{
					return true;
				}
				else {
					break;
				}
			}
			case 12: {
				if (Game_Board[i][k] == Game_Board[i + 1][k - 1]) {
					return true;
				}
				else {
					break;
				}
			}
			default: {
				if ((Game_Board[i][k] == Game_Board[i + 1][k + 1]) || (Game_Board[i][k] == Game_Board[i + 1][k - 1]))
					return true;
			}
			}
	return false;
}

//Mixing board gems(Checked)
void Mix() {
	int i = 0, k = 0, NumX = 0, NumY = 0, buff = 0;
	int Game_Board[12][12] = {};

	OpenMatrix(Game_Board);

	//Mixing itself
	for (i = 0; i < 12; i++)
		for (k = 0; k < 12; k++)
		{
			NumX = rand() % 11 + 1;
			NumY = rand() % 11 + 1;
			buff = Game_Board[i][k];
			Game_Board[i][k] = Game_Board[NumY][NumX];
			Game_Board[NumY][NumX] = buff;
		}

	//Write back matrix in file
	WriteMartix(Game_Board);
}

//Destroying the complete row(Checked)
void Destroy_Row(int Y, int X, int(&Matrix)[12][12], int Gem) {//And delete y and x from calling
	int a = 0, b = 0, NumG = 0, Row = 1;

	FILE* ScSrc = 0x00;
	unsigned long int Score = 0;
	int Game_Matr[12][12] = {};
	fopen_s(&ScSrc, "Score.txt", "r");
	if (ScSrc)
	{
		fscanf_s(ScSrc, "%u", &Score);
		fclose(ScSrc);
	}
	OpenMatrix(Game_Matr);
	
	a = Y + 1;
	b = X;
	Game_Matr[Y][X] = Gem;
	while (a != 12)
		if (Game_Matr[a][b] == Gem)
		{
			Row++;
			a++;
		}
		else
			break;
	a = Y - 1;
	while (a != -1)
		if (Game_Matr[a][b] == Gem)
		{
			Row++;
			a--;
		}
		else
			break;

	if (Row >= 3) {
		a = Y;
		while (a >= 0)
			if (a - 1 >= 0)
				if (Game_Matr[a - 1][b] == Gem)
					a--;
				else
					break;
			else
				break;

		while ((Game_Matr[a][b] == Gem) && (a != 12))
		{
			Matrix[a][b] = 0;
			a++;
		}
		NumG += Row;
	}
	Row = 1;
	a = Y;
	b = X + 1;
	while (b != 12)
		if (Game_Matr[a][b] == Gem)
		{
			Row++;
			b++;
		}
		else
			break;
	b = X - 1;
	while (b != -1)
		if (Game_Matr[a][b] == Gem)
		{
			Row++;
			b--;
		}
		else
			break;

	if (Row >= 3) {
		b = X;
		while (b >= 0)
			if (b - 1 >= 0)
				if (Game_Matr[a][b - 1] == Gem)
					b--;
				else
					break;
			else
				break;

		while ((Game_Matr[a][b] == Gem) && (b != 12))
		{
			Matrix[a][b] = 0;
			b++;
		}
		NumG += Row;
	}

	Matrix[Y][X] = 0;
	NumG++;
	switch (NumG)//Count this shit pls
	{
	case 23://1300
		Score += 4000;
		break;
	case 22://500
		Score += 2700;
		break;
	case 21://300
		Score += 2200;
		break;
	case 20://300
		Score += 1900;
		break;
	case 19://240
		Score += 1600;
		break;
	case 18://240
		Score += 1360;
		break;
	case 17://180
		Score += 1120;
		break;
	case 16://180
		Score += 940;
		break;
	case 15://140
		Score += 860;
		break;
	case 14://140
		Score += 720;
		break;
	case 13://120
		Score += 580;
		break;
	case 12://120
		Score += 460;
		break;
	case 11://80
		Score += 340;
		break;
	case 10://80
		Score += 260;
		break;
	case 9://40
		Score += 180;
		break;
	case 8://40
		Score += 140;
		break;
	case 7://20
		Score += 100;
		break;
	case 6://20
		Score += 80;
		break;
	case 5://15
		Score += 60;
		break;
	case 4://15
		Score += 45;
		break;
	case 3://10
		Score += 30;
		break;
	default:
		break;
	}
	fopen_s(&ScSrc, "Score.txt", "w+");
	if (ScSrc)
	{
		fprintf(ScSrc, "%u", Score);
		fclose(ScSrc);
	}
}

/*
Gems counting:
Default = 10
4,5  = 15	6,7 = 20	8,9 = 40	10,11 = 80	12,13 = 120 	14,15 = 140   16,17 = 180   18,19 = 240   20,21 = 400  22 = 500  23 = 1300
*/

//Make gems "fall" USE FOR ANIMATION (Checked)
void DropGems() {//Later combine with graphic
	int i = 0, j = 0, freepl = 0, filledpl = 0, buff = 0;
	int Matrix[12][12] = {};
	OpenMatrix(Matrix);
	for (i = 11; i >= 0; i--)
		for (j = 11; j >= 0; j--)
			if (Matrix[i][j] == 0)
			{
				freepl = i;
				filledpl = i;
				while ((Matrix[filledpl][j] == 0) && (filledpl != -1))
					filledpl--;
				while (filledpl != -1) {
					buff = Matrix[filledpl][j];
					Matrix[filledpl][j] = Matrix[freepl][j];
					Matrix[freepl][j] = buff;
					freepl--;
					filledpl--;
				}
			}
	WriteMartix(Matrix);
}

//Spawn new gems(Checked)
void FillVoids() {//Make it send the marker
	int i = 0, k = 0;
	int Matrix[12][12] = {};
	OpenMatrix(Matrix);
	for (i = 0; i < 12; i++)
		for (k = 0; k < 12; k++)
			if (Matrix[i][k] == 0)
				Matrix[i][k] = rand() % 5 + 1; //generating start gems
	WriteMartix(Matrix);
}

//Find all 3 in row and destroy all of them NOTE If return true then might be created new rows cuz of a 2 funcs (***) (Checked)
bool Check_Row() {
	int i = 0, k = 0, buff = 0, X = 0, Y = 0;
	bool Match = false;
	int Game_Board[12][12] = {};
	int MDestr[12][12] = {};
	OpenMatrix(Game_Board);
	OpenMatrix(MDestr);

	for (i = 11; i >= 0; i--)
		for (k = 9; k >= 0; k--)
			if ((Game_Board[i][k] == Game_Board[i][k + 1]) && (Game_Board[i][k] == Game_Board[i][k + 2]) && (Game_Board[i][k] != 0)) {
				if ((MDestr[i][k] != 0) || (MDestr[i][k + 1] != 0) || (MDestr[i][k + 2] != 0))
				{
					Destroy_Row(i, k, MDestr, Game_Board[i][k]);
					Match = true;
				}
			}
	for (k = 11; k >= 0; k--)
		for (i = 9; i >= 0; i--)
			if ((Game_Board[i][k] == Game_Board[i + 1][k]) && (Game_Board[i][k] == Game_Board[i + 2][k]) && (Game_Board[i][k] != 0)) {
				if ((MDestr[i][k] != 0) || (MDestr[i + 1][k] != 0) || (MDestr[i + 2][k] != 0))
				{
					Destroy_Row(i, k, MDestr, Game_Board[i][k]);
					Match = true;
				}
			}

	for (i = 0; i < 12; i++)
		for (k = 0; k < 12; k++)
			Game_Board[i][k] = MDestr[i][k];

	WriteMartix(Game_Board);

	return Match;
}

//Makiing move
bool Make_Move() {
	int i = 0, k = 0, buff = 0, Xold = 0, Yold = 0, Xnew = 0, Ynew = 0;

	FILE *MoveF = 0x00;
	int Game_Board[12][12] = {};

	OpenMatrix(Game_Board);
	fopen_s(&MoveF, "Dots.txt", "r");
	if (MoveF)
	{
		fscanf_s(MoveF, "%d", &buff);
		Xold = buff;
		fscanf_s(MoveF, "%d", &buff);
		Yold = buff;
		fscanf_s(MoveF, "%d", &buff);
		Xnew = buff;
		fscanf_s(MoveF, "%d", &buff);
		Ynew = buff;
		fclose(MoveF);
	}

	buff = Game_Board[Yold][Xold];
	Game_Board[Yold][Xold] = Game_Board[Ynew][Xnew];
	Game_Board[Ynew][Xnew] = buff;

	//Write back matrix in file
	WriteMartix(Game_Board);

	if (!Check_Row())
	{
		buff = Game_Board[Yold][Xold];
		Game_Board[Yold][Xold] = Game_Board[Ynew][Xnew];
		Game_Board[Ynew][Xnew] = buff;

		//Write back matrix in file
		WriteMartix(Game_Board);

		return false;
	}
	return true;
}

//Create new board & mix it (Checked)
void StartGame() {
	int i = 0, k = 0, buff = 0;
	int Game_Board[12][12] = {};
	FILE* Src = 0x00;
	fopen_s(&Src, "GameDesk.txt", "w+");//Maybe different type of open
	//Somehow read info from file
	if (Src)
	{
		for (i = 0; i < 12; i++)
			for (k = 0; k < 12; k++)
			{
				Game_Board[i][k] = rand() % 5 + 1;
				fprintf(Src, "%u\n", Game_Board[i][k]);
			}
		fclose(Src);
		while (!Check_Any_Move()) {
			Mix();
			Check_Board();
		}
	}

}
