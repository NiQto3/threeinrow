﻿// WindowsProject2.cpp : Определяет точку входа для приложения.
//

#include "framework.h"
#include "WindowsProject2.h"
#include "Draw.h"
#include "BackHead.h"
#include <iostream>

#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

HDC hdcBack;
RECT clientSize;
#define DelayT 400

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Разместите код здесь.

    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_WINDOWSPROJECT2, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT2));

    MSG msg;

    // Цикл основного сообщения:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = 0;
    wcex.lpszMenuName   = 0;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
 
    return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

   HWND hWnd = CreateWindowW(szWindowClass, L"Gems", WS_POPUP
       , 0, 0, 1920, 1080, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, SW_SHOWMAXIMIZED);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    
    switch (message)
    {
    case WM_CREATE:
        {
            //Game start button
            HWND hNGButton = CreateWindow(
                L"Button",
                L"New Game",
                WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
                1600, 200, 200, 50, hWnd, reinterpret_cast<HMENU>(1001), nullptr, nullptr
            );            

            //Save game button
            HWND hSGButton = CreateWindow(
                L"Button",
                L"Save game",
                WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
                1600, 300, 200, 50, hWnd, reinterpret_cast<HMENU>(1002), nullptr, nullptr
            );

            //Load game button
            HWND hLGButton = CreateWindow(
                L"Button",
                L"Load Game",
                WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
                1600, 400, 200, 50, hWnd, reinterpret_cast<HMENU>(1003), nullptr, nullptr
            );

            //Quit button
            HWND hQButton = CreateWindow(
                L"Button",
                L"Exit",
                WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
                1600, 500, 200, 50, hWnd, reinterpret_cast<HMENU>(1004), nullptr, nullptr
            );

            HDC hdc = GetDC(hWnd);
           
            GetClientRect(hWnd, &clientSize);
            hdcBack = CreateCompatibleDC(hdc);
            HBITMAP hbtm = CreateCompatibleBitmap(hdc, clientSize.right, clientSize.bottom);
            SaveDC(hdcBack);
            SelectObject(hdcBack, hbtm);
            ReleaseDC(hWnd, hdc);

            /*//test
            HWND hTest = CreateWindow(
                L"Static",
                NULL,
                WS_CHILD | WS_VISIBLE | BS_TEXT,
                0, 0, 200, 50, hWnd, reinterpret_cast<HMENU>(1004), nullptr, nullptr
            );*/
        }
     return 0;

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Разобрать выбор в меню:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;

            //That button will start game
            case 1001:
            {
                RECT r{ 480, 0, 1440, 1040 };
                StartGame();
                Check_Board();
                Check_Board();
                FILE* Src = 0x00;
                fopen_s(&Src, "Dots.txt", "w+");
                if (Src)
                {
                    fclose(Src);
                }
                fopen_s(&Src, "Score.txt", "w+");
                if (Src)
                {
                    fclose(Src);
                }
                InvalidateRect(hWnd, &r, FALSE); //This will cause the redraw of the board BUT you need to focus it on one place                
            }
            break;

            //That button will save game
            case 1002:
            {
                FILE* FSave = 0x00, * FWorkW = 0x00;
                int  buff = 0;
                unsigned long int a = 0;
                fopen_s(&FSave, "SaveGame.txt", "w+");
                fopen_s(&FWorkW, "GameDesk.txt", "r");
                if (FSave && FWorkW)
                {
                    fscanf_s(FWorkW, "%d", &buff);
                    while (!feof(FWorkW)) {                      
                        fprintf(FSave, "%d\n", buff);
                        fscanf_s(FWorkW, "%d", &buff);
                    }
                    fclose(FWorkW);
                }
                fopen_s(&FWorkW, "Score.txt", "r");
                if (FSave && FWorkW)
                {
                    fscanf_s(FWorkW, "%u", &a);
                    fprintf(FSave, "%u\n", a);
                    fclose(FWorkW);
                    fclose(FSave);
                }
            }
            break;

            //That button will load game
            case 1003:
            {
                FILE* FSave = 0x00, * FWorkW = 0x00;
                RECT r{ 480, 0, 1440, 1040 };
                int  buff = 0, a = 0;
                unsigned long int b = 0;
                fopen_s(&FSave, "SaveGame.txt", "r");
                fopen_s(&FWorkW, "GameDesk.txt", "w+");
                if (FSave)
                {
                    fscanf_s(FSave, "%d", &buff);
                    if (!feof(FSave))
                    {
                        fclose(FSave);
                        fopen_s(&FSave, "SaveGame.txt", "r");
                        if (FSave && FWorkW)
                        {
                            for (a = 0; a < 144; a++)
                            {
                                fscanf_s(FSave, "%d", &buff);
                                fprintf(FWorkW, "%d\n", buff);
                            }
                            fclose(FWorkW);
                        }
                        fopen_s(&FWorkW, "Score.txt", "w+");
                        if (FSave && FWorkW)
                        {
                            fscanf_s(FSave, "%u", &b);
                            fprintf(FWorkW, "%u\n", b);
                            fclose(FWorkW);
                            fclose(FSave);
                        }
                        InvalidateRect(hWnd, &r, FALSE); //This will cause the redraw of the board BUT you need to focus it on one place 
                    }
                    else {
                        fclose(FSave);
                        fclose(FWorkW);
                    }                        
                }
               
            }
            break;

            //That button will exit the program
            case 1004:
                FILE * Src;
                fopen_s(&Src, "Dots.txt", "w+");
                if (Src)
                {
                    fclose(Src);
                }
                PostQuitMessage(0);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Добавьте сюда любой код прорисовки, использующий HDC...
            //brush = CreateHatchBrush(HS_HORIZONTAL, RGB(0, 255, 0));
            DrawBackgrownd(hdcBack);
            DrawMatrix(hdcBack);
            BitBlt(hdc, 0, 0, clientSize.right, clientSize.bottom, hdcBack, 0, 0, SRCCOPY);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    
     //Write how to ssend this coord to functions

    case WM_LBUTTONDOWN:
        {
            if (wParam == VK_LBUTTON) {
                POINT dot;
                FILE* FDot;
                int X = 0, Y = 0, TX = 0, TY = 0, i = 0, k = 0;
                GetCursorPos(&dot);
                X = dot.x;
                Y = dot.y; 
                if ((X >= 480) && (X <= 1440) && (Y >= 80) && (Y <= 1040))
                {
                    fopen_s(&FDot, "Dots.txt", "r+");
                    if (FDot)
                    {
                        fscanf_s(FDot, "%d", &TX);
                        if (!feof(FDot))
                        {
                            fscanf_s(FDot, "%d", &TY);
                            i = 80;
                            while (Y > i)
                                i += 80;
                            k = 480;
                            while (X > k)
                                k += 80;
                            i = (i - 80) / 80;
                            i--;
                            k = (k - 480) / 80;
                            k--;                            
                            if( ( (k == TX) && ((i == TY + 1) || (i == TY - 1) ) ) || ( (i == TY) && (k == TX - 1) || (k == TX + 1) ) )
                            {
                                fprintf(FDot, "\n");
                                fprintf(FDot, "%d\n", k);
                                fprintf(FDot, "%d\n", i);
                                fclose(FDot);
                                int CMatr[12][12] = {};
                                int CHMatr[12][12] = {};
                                bool Ch = false;
                                OpenMatrix(CMatr);
                                Make_Move();//All rows are destroyed
                                OpenMatrix(CHMatr);
                                for (i = 0; i < 12; i++)
                                    for (k = 0; k < 12; k++)
                                        if (CMatr[i][k] != CHMatr[i][k])
                                            Ch = true;                                 
                                if (Ch)
                                {
                                    RECT r{ 480, 0, 1440, 1040 };
                                    InvalidateRect(hWnd, &r, FALSE); //This will cause the redraw of the board BUT you need to focus it on one place
                                    UpdateWindow(hWnd);
                                    Sleep(DelayT);
                                    DropGems();
                                    InvalidateRect(hWnd, &r, FALSE);
                                    UpdateWindow(hWnd);
                                    Sleep(DelayT);
                                    FillVoids();
                                    InvalidateRect(hWnd, &r, FALSE);
                                    UpdateWindow(hWnd);
                                    Sleep(DelayT);
                                    while (Check_Row()) {
                                        InvalidateRect(hWnd, &r, FALSE);
                                        UpdateWindow(hWnd);
                                        Sleep(DelayT);
                                        DropGems();
                                        InvalidateRect(hWnd, &r, FALSE);
                                        UpdateWindow(hWnd);
                                        Sleep(DelayT);
                                        FillVoids();
                                        InvalidateRect(hWnd, &r, FALSE);
                                        UpdateWindow(hWnd);
                                        Sleep(DelayT);
                                    }                                    
                                }   
                                fopen_s(&FDot, "Dots.txt", "w+");
                                if (FDot)
                                {
                                    fclose(FDot);
                                    InvalidateRect(hWnd, &clientSize, FALSE);
                                    UpdateWindow(hWnd);
                                }

                                    /*   
                                    fopen_s(&FDot, "Dots.txt", "w+");
                                    if (FDot)
                                        fclose(FDot);
                                    FILE* Src;
                                    int buff = 0;
                                    int Matr[12][12] = {};
                                    fopen_s(&Src, "GameDesk.txt", "r");
                                    for ( i = 0; i < 12; i++)
                                        for ( k = 0; k < 12; k++)                                        
                                            fscanf_s(Src, "%d", &buff);
                                    Matr[i][k] = buff;
                                    fclose(Src);
                                    DropGems(Matr);
                                    fopen_s(&Src, "GameDesk.txt", "w+");
                                    for (i = 0; i < 12; i++)
                                        for (k = 0; k < 12; k++)
                                            fprintf(Src, "%d\n", &buff);
                                    Matr[i][k] = buff;
                                    fclose(Src);
                                    InvalidateRect(hWnd, NULL, FALSE); //This will cause the redraw of the board BUT you need to focus it on one place  
                                    Sleep(1000);
                                    FillVoids(Matr);                                   
                                    fopen_s(&Src, "GameDesk.txt", "w+");
                                    for (i = 0; i < 12; i++)
                                        for (k = 0; k < 12; k++)
                                            fprintf(Src, "%d\n", &Matr[i][k]);
                                    fclose(Src);
                                    InvalidateRect(hWnd, NULL, FALSE); //This will cause the redraw of the board BUT you need to focus it on one place  
                                    Sleep(1000);*/
                                
                            }
                            else {
                                fclose(FDot);
                                fopen_s(&FDot, "Dots.txt", "w+");
                                if (FDot)
                                {
                                    fprintf(FDot, "%d\n", k);
                                    fprintf(FDot, "%d\n", i);
                                    fclose(FDot);
                                }   
                                InvalidateRect(hWnd, &clientSize, FALSE);
                                UpdateWindow(hWnd);
                            }
                        }
                        else {
                            fclose(FDot);
                            fopen_s(&FDot, "Dots.txt", "w");
                            if (FDot)
                            {
                                i = 80;
                                while (Y > i)
                                    i += 80;
                                k = 480;
                                while (X > k)
                                    k += 80;
                                i = (i - 80) / 80;
                                i--;
                                k = (k - 480) / 80;
                                k--;
                                fprintf(FDot, "%d\n", k);
                                fprintf(FDot, "%d\n", i);
                                fclose(FDot);                                                               
                                InvalidateRect(hWnd, &clientSize, FALSE);
                                UpdateWindow(hWnd);
                            }
                        }
                    }
                } 
            }
        }
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;

}

// Обработчик сообщений для окна "О программе".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
